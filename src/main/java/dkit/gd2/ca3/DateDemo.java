package dkit.gd2.ca3;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDemo {
    public static void main(String[] args) {
        printCurrentDateAndTime();
        printSimpleDateFormat();
        printPrintfDate();
    }

    public static void printCurrentDateAndTime(){
        Date date = new Date();
        System.out.println(date.toString());
    }

    public static void printSimpleDateFormat(){
        Date now = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
        System.out.println("Current Date: " + ft.format(now));
    }

    public static void printPrintfDate(){
        Date date = new Date();
        System.out.printf("%1$s %2$tB %2$td, %2$tY\n", "Due date:", date);
    }

    
}
