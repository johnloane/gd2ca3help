package dkit.gd2.ca3;

import java.io.*;

public class SerializeDeserializeDemo {

    public static void main(String[] args) {
        Employee patrick = new Employee("Patrick Fitspatrick", "Dundalk, Ireland", 1234567, 101);
        serializeEmployee(patrick);

        Employee er = deserializeEmployee();

        System.out.println("Deserializing employee");
        System.out.println(er.toString());
    }

    public static void serializeEmployee(Employee e){
        try{
            FileOutputStream fileOut = new FileOutputStream("employee.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(e);
            out.close();
            System.out.println("Serialized data is saved to employee.ser");
        }
        catch(IOException io){
            io.printStackTrace();
        }
    }

    public static Employee deserializeEmployee(){
        Employee el = null;
        try{
            FileInputStream fileIn = new FileInputStream("employee.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            el = (Employee) in.readObject();
            in.close();
            return el;
        }catch(IOException io){
            io.printStackTrace();
        } catch(ClassNotFoundException c){
            System.out.println("Employee class not found");
            c.printStackTrace();
        }
        return el;
    }
}
